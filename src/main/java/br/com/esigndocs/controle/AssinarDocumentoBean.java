package br.com.esigndocs.controle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.SystemException;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

import br.com.esigndocs.modelo.BtpAnexoAssinatura;
import br.com.esigndocs.modelo.BtpUsuario;
import br.com.esigndocs.negocio.NgcAssinarDocumento;
import br.com.esigndocs.utils.Constants;
import br.com.esigndocs.utils.FacesUtils;

@Named
@SessionScoped	
public class AssinarDocumentoBean implements Serializable {

	private static final long serialVersionUID = -8183264622077888688L;

	@Inject
	private NgcAssinarDocumento ngcAssinarDocumento;

	@Inject
	private BtpAnexoAssinatura btpAnexoAssinatura;
	
	@Inject
	private SessaoBean sessaoBean;
	
	private List<BtpAnexoAssinatura> lstBtpAnexoAssinatura;
	
	private BtpUsuario btpUsuarioLogado;

	private byte[] anexo;
	
	@PostConstruct
	public void init() {
		this.btpUsuarioLogado = new BtpUsuario();
		this.btpUsuarioLogado = sessaoBean.getBtpUsuarioAutenticado();
		this.lstBtpAnexoAssinatura = new ArrayList<>();
		consultarListaAnexoUsuario(btpUsuarioLogado.getUsuCodUsuario());
	}

	public void enviarArquivo(FileUploadEvent event) {
		UploadedFile uploadedFile = event.getFile();
		btpAnexoAssinatura = new BtpAnexoAssinatura();
		String nomeArquivo = null;
		String dataHoraUpload = null;
		String nomeFormatado = null;

		try (InputStream inputStream = uploadedFile.getInputStream()) {
			if (uploadedFile.getFileName() != null) {
				nomeArquivo = uploadedFile.getFileName().substring(0, uploadedFile.getFileName().length() - 4);
				dataHoraUpload = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
				nomeArquivo = nomeArquivo.replaceAll("[^a-zA-Z0-9.-]", "_");
				nomeArquivo = nomeArquivo.replaceAll("\\s+", "");
				nomeFormatado = nomeArquivo + "_" + dataHoraUpload + ".pdf";
			}
			File arquivoDestino = new File(Constants.PATH_DIRETORIO_SRVARQ + File.separator + nomeFormatado);
			try (FileOutputStream outputStream = new FileOutputStream(arquivoDestino)) {
				byte[] buffer = new byte[2048];
				int bytesRead;

				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}

				btpAnexoAssinatura.setAnaDscNome(nomeFormatado);
				btpAnexoAssinatura.setAnaDatDataEnvio(new Date());
				btpAnexoAssinatura.setUsuCodUsuario(btpUsuarioLogado.getUsuCodUsuario());
				ngcAssinarDocumento.gravarAnexoBanco(btpAnexoAssinatura);
				anexo = null;
				consultarListaAnexoUsuario(btpUsuarioLogado.getUsuCodUsuario());
				FacesUtils.addMensagemInformacao("Arquivo enviado com sucesso!");
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			FacesUtils.addMensagemErro("Não foi possível enviar o arquivo!");
			e.printStackTrace();
		}
	}
	
	private void consultarListaAnexoUsuario(Long usuCodUsuario) {
		lstBtpAnexoAssinatura = ngcAssinarDocumento.consultarAnexoUsuario(usuCodUsuario);
	}
	
	public void obterDadosUsuarioAutenticado(BtpUsuario usuarioAutenticado) {
		this.btpUsuarioLogado = usuarioAutenticado;
	}
	
	public byte[] getAnexo() {
		return anexo;
	}

	public void setAnexo(byte[] anexo) {
		this.anexo = anexo;
	}

	public List<BtpAnexoAssinatura> getLstBtpAnexoAssinatura() {
		return lstBtpAnexoAssinatura;
	}

	public void setLstBtpAnexoAssinatura(List<BtpAnexoAssinatura> lstBtpAnexoAssinatura) {
		this.lstBtpAnexoAssinatura = lstBtpAnexoAssinatura;
	}

	public BtpUsuario getBtpUsuarioLogado() {
		return btpUsuarioLogado;
	}

	public void setBtpUsuarioLogado(BtpUsuario btpUsuarioLogado) {
		this.btpUsuarioLogado = btpUsuarioLogado;
	}

}
