package br.com.esigndocs.controle;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.esigndocs.utils.FacesUtils;

@Named
@SessionScoped
public class MenuBean implements Serializable{

	private static final long serialVersionUID = 3119191707645092014L;
	
	public void goToInicio() {
        redirectTo("inicio.xhtml");
    }

    public void goToAssign() {
        redirectTo("assign/assinarDocumento.xhtml");
    }

    public void goToAuthAssign() {
        redirectTo("auth/autenticarAssinatura.xhtml");
    }

    public void goToAbout() {
        redirectTo("about/sobre.xhtml");
    }

    public void goToLogin() {
        redirectTo("login/iniciarSessao.xhtml");
    }

    public void goToRegister() {
        redirectTo("login/registro.xhtml");
    }

    public void goToPrivacy() {
        redirectTo("termsAndPolitics/politicaPrivacidade.xhtml");
    }

    public void goToTermUser() {
        redirectTo("termsAndPolitics/termos.xhtml");
    }

    public void goToHelp() {
        redirectTo("about/ajuda.xhtml");
    }

    public void goToDocTypes() {
        redirectTo("about/formatos.xhtml");
    }

    private void redirectTo(String page) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        try {
        	String contexto = externalContext.getRequestContextPath();
			externalContext.redirect(contexto + "/faces/pages/" + page);
		} catch (Exception e) {
			FacesUtils.addMensagemErro("Não foi possível acessar a pagina de destino!");
		}
    }
}
