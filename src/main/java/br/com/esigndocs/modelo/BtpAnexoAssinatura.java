package br.com.esigndocs.modelo;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ANA_ANEXO_ASSINATURA")
@SequenceGenerator(name = "SEQ_ANA_ANEXO_ASSINATURA", sequenceName = "SEQ_ANA_ANEXO_ASSINATURA", allocationSize = 1)
public class BtpAnexoAssinatura implements Serializable{

	private static final long serialVersionUID = -554338499707693239L;
	
	/**
	 * @author Donatti 22/09/2023 - 22:47:09
	 */

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ANA_ANEXO_ASSINATURA")
    @Column(name = "ANA_COD_ANEXO")
    private Long anaCodAnexo;

    @Column(name = "ANA_DSC_NOME")
    private String anaDscNome;

    @Column(name = "ANA_DAT_DATA_ENVIO")
    private Date anaDatDataEnvio;

    @Column(name = "ANA_DAT_ASSINATURA")
    private Date anaDatAssinatura;

    @Column(name = "HAA_COD_HASH")
    private Long haaCodHash;

    @Column(name = "USU_COD_USUARIO")
    private Long usuCodUsuario;

	public Long getAnaCodAnexo() {
		return anaCodAnexo;
	}

	public void setAnaCodAnexo(Long anaCodAnexo) {
		this.anaCodAnexo = anaCodAnexo;
	}

	public String getAnaDscNome() {
		return anaDscNome;
	}

	public void setAnaDscNome(String anaDscNome) {
		this.anaDscNome = anaDscNome;
	}

	public Date getAnaDatDataEnvio() {
		return anaDatDataEnvio;
	}

	public void setAnaDatDataEnvio(Date anaDatDataEnvio) {
		this.anaDatDataEnvio = anaDatDataEnvio;
	}

	public Date getAnaDatAssinatura() {
		return anaDatAssinatura;
	}

	public void setAnaDatAssinatura(Date anaDatAssinatura) {
		this.anaDatAssinatura = anaDatAssinatura;
	}

	public Long getHaaCodHash() {
		return haaCodHash;
	}

	public void setHaaCodHash(Long haaCodHash) {
		this.haaCodHash = haaCodHash;
	}

	public Long getUsuCodUsuario() {
		return usuCodUsuario;
	}

	public void setUsuCodUsuario(Long usuCodUsuario) {
		this.usuCodUsuario = usuCodUsuario;
	}

	public BtpAnexoAssinatura() {

	}

	public BtpAnexoAssinatura(Long anaCodAnexo, String anaDscNome, Date anaDatDataEnvio, Date anaDatAssinatura,
			Long haaCodHash, Long usuCodUsuario) {
		this.anaCodAnexo = anaCodAnexo;
		this.anaDscNome = anaDscNome;
		this.anaDatDataEnvio = anaDatDataEnvio;
		this.anaDatAssinatura = anaDatAssinatura;
		this.haaCodHash = haaCodHash;
		this.usuCodUsuario = usuCodUsuario;
	}
    
}