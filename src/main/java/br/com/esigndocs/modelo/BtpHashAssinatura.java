package br.com.esigndocs.modelo;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "HAA_HASH_ASSINATURA")
public class BtpHashAssinatura implements Serializable{

	private static final long serialVersionUID = 507473844630425014L;
	
	/**
	 * @author Donatti 22/09/2023 - 22:41:58
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HAA_COD_HASH")
	private Long haaCodHash;

	@Column(name = "HAA_DSC_HASH_ASSINATURA")
	private String haaDscHashAssinatura;

	@Column(name = "HAA_DAT_CRIACAO")
	private Date haaDataCriacao;
	
	public Long getHaaCodHash() {
		return haaCodHash;
	}

	public void setHaaCodHash(Long haaCodHash) {
		this.haaCodHash = haaCodHash;
	}

	public String getHaaDscHashAssinatura() {
		return haaDscHashAssinatura;
	}

	public void setHaaDscHashAssinatura(String haaDscHashAssinatura) {
		this.haaDscHashAssinatura = haaDscHashAssinatura;
	}

	public Date getHaaDataCriacao() {
		return haaDataCriacao;
	}

	public void setHaaDataCriacao(Date haaDataCriacao) {
		this.haaDataCriacao = haaDataCriacao;
	}

	public BtpHashAssinatura() {
	}
	
	public BtpHashAssinatura(Long haaCodHash) {
		this.haaCodHash = haaCodHash;
	}

	public BtpHashAssinatura(Long haaCodHash, String haaDscHashAssinatura, Date haaDataCriacao) {
		this.haaCodHash = haaCodHash;
		this.haaDscHashAssinatura = haaDscHashAssinatura;
		this.haaDataCriacao = haaDataCriacao;
	}
	
}
