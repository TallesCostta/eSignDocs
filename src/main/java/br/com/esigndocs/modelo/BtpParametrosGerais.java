package br.com.esigndocs.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BtpParametrosGerais implements Serializable{

	private static final long serialVersionUID = -245390263358575101L;
	
	@Id
	@Column(name = "CHAVE")
	private String chave;
	
	@Column(name = "VALOR")
	private String valor;

	public BtpParametrosGerais() {
	}

	public BtpParametrosGerais(String chave, String valor) {
		this.chave = chave;
		this.valor = valor;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
