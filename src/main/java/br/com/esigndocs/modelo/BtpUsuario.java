package br.com.esigndocs.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "USU_USUARIO")
@SequenceGenerator(name = "SEQ_USU_USUARIO", sequenceName = "SEQ_USU_USUARIO", allocationSize = 1)
public class BtpUsuario implements Serializable {

	private static final long serialVersionUID = -8871794667305744188L;

	/**
	 * @author Donatti 22/09/2023 - 22:40:18
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USU_USUARIO")
	@Column(name = "USU_COD_USUARIO")
	private Long usuCodUsuario;

	@Column(name = "USU_DSC_NOME")
	private String usuDscNome;

	@Column(name = "USU_DSC_EMAIL")
	private String usuDscEmail;

	@Column(name = "USU_NUM_TELEFONE")
	private String usuNumTelefone;

	@Column(name = "USU_DSC_CPF")
	private String usuDscCpf;

	@Column(name = "USU_DAT_NASCIMENTO")
	private String usuDatNascimento;

	@Column(name = "USU_DSC_LOGIN")
	private String usuDscLogin;

	@Column(name = "USU_DSC_SENHA")
	private String usuDscSenha;

	@Column(name = "USU_DSC_ENDERECO")
	private String usuDscEndereco;

	@Column(name = "USU_DSC_CIDADE")
	private String usuDscCidade;

	@Column(name = "USU_DSC_BAIRRO")
	private String usuDscBairro;

	@Column(name = "USU_DSC_UF")
	private String usuDscUf;

	public Long getUsuCodUsuario() {
		return usuCodUsuario;
	}

	public void setUsuCodUsuario(Long usuCodUsuario) {
		this.usuCodUsuario = usuCodUsuario;
	}

	public String getUsuDscNome() {
		return usuDscNome;
	}

	public void setUsuDscNome(String usuDscNome) {
		this.usuDscNome = usuDscNome;
	}

	public String getUsuDscEmail() {
		return usuDscEmail;
	}

	public void setUsuDscEmail(String usuDscEmail) {
		this.usuDscEmail = usuDscEmail;
	}

	public String getUsuNumTelefone() {
		return usuNumTelefone;
	}

	public void setUsuNumTelefone(String usuNumTelefone) {
		this.usuNumTelefone = usuNumTelefone;
	}

	public String getUsuDscCpf() {
		return usuDscCpf;
	}

	public void setUsuDscCpf(String usuDscCpf) {
		this.usuDscCpf = usuDscCpf;
	}

	public String getUsuDatNascimento() {
		return usuDatNascimento;
	}

	public void setUsuDatNascimento(String usuDatNascimento) {
		this.usuDatNascimento = usuDatNascimento;
	}

	public String getUsuDscLogin() {
		return usuDscLogin;
	}

	public void setUsuDscLogin(String usuDscLogin) {
		this.usuDscLogin = usuDscLogin;
	}

	public String getUsuDscSenha() {
		return usuDscSenha;
	}

	public void setUsuDscSenha(String usuDscSenha) {
		this.usuDscSenha = usuDscSenha;
	}

	public String getUsuDscEndereco() {
		return usuDscEndereco;
	}

	public void setUsuDscEndereco(String usuDscEndereco) {
		this.usuDscEndereco = usuDscEndereco;
	}

	public String getUsuDscCidade() {
		return usuDscCidade;
	}

	public void setUsuDscCidade(String usuDscCidade) {
		this.usuDscCidade = usuDscCidade;
	}

	public String getUsuDscBairro() {
		return usuDscBairro;
	}

	public void setUsuDscBairro(String usuDscBairro) {
		this.usuDscBairro = usuDscBairro;
	}

	public String getUsuDscUf() {
		return usuDscUf;
	}

	public void setUsuDscUf(String usuDscUf) {
		this.usuDscUf = usuDscUf;
	}

	public BtpUsuario() {

	}

	public BtpUsuario(Long usuCodUsuario) {
		this.usuCodUsuario = usuCodUsuario;
	}

	public BtpUsuario(String usuDscLogin, String usuDscSenha) {
		this.usuDscLogin = usuDscLogin;
		this.usuDscSenha = usuDscSenha;
	}

	public BtpUsuario(Long usuCodUsuario, String usuDscNome, String usuDscEmail, String usuNumTelefone,
			String usuDscCpf, String usuDatNascimento, String usuDscLogin, String usuDscSenha, String usuDscEndereco,
			String usuDscCidade, String usuDscBairro, String usuDscUf) {
		this.usuCodUsuario = usuCodUsuario;
		this.usuDscNome = usuDscNome;
		this.usuDscEmail = usuDscEmail;
		this.usuNumTelefone = usuNumTelefone;
		this.usuDscCpf = usuDscCpf;
		this.usuDatNascimento = usuDatNascimento;
		this.usuDscLogin = usuDscLogin;
		this.usuDscSenha = usuDscSenha;
		this.usuDscEndereco = usuDscEndereco;
		this.usuDscCidade = usuDscCidade;
		this.usuDscBairro = usuDscBairro;
		this.usuDscUf = usuDscUf;
	}

}
