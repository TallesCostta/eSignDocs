package br.com.esigndocs.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.transaction.SystemException;

import br.com.esigndocs.modelo.BtpAnexoAssinatura;

@Stateless
public class NgcAssinarDocumento extends NgcPadraoBean<BtpAnexoAssinatura> {

    public NgcAssinarDocumento() {
        super(BtpAnexoAssinatura.class);
    }
    
	public void gravarAnexoBanco(BtpAnexoAssinatura anexo) throws IllegalStateException, SecurityException, SystemException {
		if (anexo != null) {
			anexo = getEm().merge(anexo);
			salvar(anexo);
		}
	}
    
    @SuppressWarnings("unchecked")
	public List<BtpAnexoAssinatura> consultarAnexoUsuario(Long usuCodUsuario){
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append(" SELECT NEW BtpAnexoAssinatura( ");
    	sb.append(" btpAna.anaCodAnexo, btpAna.anaDscNome, ");
    	sb.append(" btpAna.anaDatDataEnvio, btpAna.anaDatAssinatura, ");
    	sb.append(" btpAna.haaCodHash, btpAna.usuCodUsuario )");
    	sb.append(" FROM BtpAnexoAssinatura btpAna ");
    	sb.append(" WHERE btpAna.usuCodUsuario = :usuCodUsuario ");
    	
    	Query query = getEm().createQuery(sb.toString());
    	
    	query.setParameter("usuCodUsuario", usuCodUsuario);
    	
    	return query.getResultList();
    }
}
