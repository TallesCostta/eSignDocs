package br.com.esigndocs.negocio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

public abstract class NgcPadraoBean<T> {

	@PersistenceContext
	private EntityManager em;

    private final Class<T> entityClass;

    public NgcPadraoBean(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Transactional
    public T salvar(T entidade) {
        em.persist(entidade);
        return entidade;
    }

    @Transactional
    public T atualizar(T entidade) {
        return em.merge(entidade);
    }

    @Transactional
    public void excluir(T entidade) {
        em.remove(em.contains(entidade) ? entidade : em.merge(entidade));
    }

    public T encontrarPorId(Long id) {
        return em.find(entityClass, id);
    }

    public List<T> listarTodos() {
        return em.createQuery("SELECT e FROM " + entityClass.getSimpleName() + " e", entityClass).getResultList();
    }

    public List<T> consultarPorQuery(StringBuilder sb) {
        return em.createQuery(sb.toString(), entityClass).getResultList();
    }

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
    
}

