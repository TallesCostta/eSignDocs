package br.com.esigndocs.utils;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

public class PersistenceInterceptor {

	@PersistenceContext
	private EntityManager em;

	@AroundInvoke
	public Object intercept(InvocationContext context) throws Exception {
		Object result = context.proceed();

		if (context.getMethod().isAnnotationPresent(Transactional.class)) {
			em.flush();
		}

		return result;
	}
}
